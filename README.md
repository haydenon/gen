# Gen

A data generation project for with the following aims:

* Easy to define new entities/resources and constraints on their inputs
* Any random resource can be created without specifiying any specific inputs, including a full tree of dependent resources
* Simple and quick way to create random data for specifc scenarios

## Testing

Testing and experimentation with the API of the project is done via the [gen-test project](https://gitlab.com/haydenon/gen-test).
