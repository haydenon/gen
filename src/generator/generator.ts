import { PropertyMap, PropertyValues } from '../resources/resource';
import {
  PropertyTypeVisitor,
  ArrayType,
  ComplexType,
  Nullable,
  Undefinable,
  acceptPropertyType,
} from '../resources/properties';
import { ResourceInstance } from '../resources/instance';
import { DesiredState } from '../resources/desired-state';
import { fillInDesiredStateTree } from './property-generation';

const DEFAULT_CREATE_TIMEOUT = 30 * 1000;

interface StateNode {
  state: DesiredState;
  depth: number;
  dependencies: StateNode[];
  depedendents: StateNode[];
  output?: ResourceInstance<PropertyMap>;
  error?: GenerationError;
}

export class ResourceLink {
  constructor(
    public item: DesiredState,
    public outputAccessor: (outputs: PropertyValues<PropertyMap>) => any
  ) {}
}
export const isResourceLink = (value: any): value is ResourceLink =>
  value instanceof ResourceLink;

const CONCURRENT_CREATIONS = 10;

interface GeneratorOptions {
  onCreate?: (resource: ResourceInstance<PropertyMap>) => void;
  onError?: (error: GenerationError) => void;
}

const getNode =
  (stateNodes: StateNode[]) =>
  (state: DesiredState): StateNode => {
    const node = stateNodes.find((n) => n.state === state);
    if (!node) {
      throw new Error('No node for state');
    }
    return node;
  };

class LinkFillVisitor implements PropertyTypeVisitor<any> {
  constructor(
    private value: any,
    private getLinkValue: (resourceLink: ResourceLink) => any
  ) {}

  visitBool = () => this.value;
  visitNum = () => this.value;
  visitStr = () => this.value;
  visitArray = (type: ArrayType) => {
    const arr = this.value as any[];
    const innerType = type.inner;
    const result: any[] = arr.map((item) => {
      this.value = item;
      return acceptPropertyType(this, innerType);
    });
    this.value = arr;
    return result;
  };
  visitNull = (type: Nullable): any =>
    this.value === null ? null : acceptPropertyType(this, type.inner);
  visitUndefined = (type: Undefinable): any =>
    this.value === undefined ? undefined : acceptPropertyType(this, type.inner);
  visitComplex = (type: ComplexType) => {
    const fields = type.fields;
    const originalValue = this.value;
    const result = Object.keys(this.value).reduce((acc, key) => {
      this.value = originalValue[key];
      acc[key] = acceptPropertyType(this, fields[key]);
      return acc;
    }, {} as any);
    this.value = originalValue;
    return result;
  };
  visitLink = () => {
    if (isResourceLink(this.value) && this.value) {
      return this.getLinkValue(this.value);
    }

    return this.value;
  };
}

class ResourceLinkVisitor implements PropertyTypeVisitor<ResourceLink[]> {
  constructor(private value: any) {}

  visitBool = () => [];
  visitNum = () => [];
  visitStr = () => [];
  visitArray = (type: ArrayType) => {
    const arr = this.value as any[];
    const innerType = type.inner;
    const result: ResourceLink[] = arr.reduce((acc, item) => {
      this.value = item;
      return [...acc, ...acceptPropertyType<ResourceLink[]>(this, innerType)];
    }, [] as ResourceLink[]);
    this.value = arr;
    return result;
  };
  visitNull = (type: Nullable): ResourceLink[] =>
    this.value === null ? [] : acceptPropertyType(this, type.inner);
  visitUndefined = (type: Undefinable): ResourceLink[] =>
    this.value === undefined
      ? []
      : acceptPropertyType<ResourceLink[]>(this, type.inner);
  visitComplex = (type: ComplexType) => {
    const fields = type.fields;
    const originalValue = this.value;
    const result: ResourceLink[] = Object.keys(this.value).reduce(
      (acc, key) => {
        this.value = originalValue[key];
        return [
          ...acc,
          ...acceptPropertyType<ResourceLink[]>(this, fields[key]),
        ];
      },
      [] as ResourceLink[]
    );
    this.value = originalValue;
    return result;
  };
  visitLink = () => (isResourceLink(this.value) ? [this.value] : []);
}

export class Generator {
  private inProgressCount = 0;
  private queued: StateNode[] = [];
  private resolve: (results: ResourceInstance<PropertyMap>[]) => void;
  private reject: (error: Error) => void;
  private promise: Promise<ResourceInstance<PropertyMap>[]>;

  private constructor(
    private stateNodes: StateNode[],
    private options?: GeneratorOptions
  ) {
    this.appendReadyNodesToQueue(stateNodes);
    this.resolve = () => {};
    this.reject = () => {};
    this.promise = new Promise((res, rej) => {
      this.resolve = res;
      this.reject = rej;
    });
  }

  async generateState(): Promise<ResourceInstance<PropertyMap>[]> {
    this.runRound();
    return this.promise;
  }

  private runRound(): void {
    // TODO: Determine when finished
    const toCreate = this.getNextForCreation(CONCURRENT_CREATIONS);
    if (toCreate.length <= 0) {
      if (!this.anyInProgress) {
        this.completeGeneration();
      }
      return;
    }

    for (const stateItem of toCreate) {
      this.markCreating(stateItem);
      this.createDesiredState(stateItem)
        .then((instance) => {
          this.notifyItemSuccess(instance);
          this.markCreated(stateItem, instance);
          this.runRound();
        })
        .catch((err: Error) => {
          this.markFailed(stateItem, err);
          this.notifyItemError(err, stateItem);
          this.runRound();
        });
    }
  }

  private async createDesiredState(
    state: DesiredState
  ): Promise<ResourceInstance<PropertyMap>> {
    return new Promise((res, rej) => {
      const timeout =
        state.resource.createTimeoutMillis ?? DEFAULT_CREATE_TIMEOUT;
      const timerId = setTimeout(() => {
        rej(
          new Error(
            `Creating desired state item '${state.name}' of resource '${state.resource.constructor.name}' timed out`
          )
        );
      }, timeout);
      const created = state.resource
        .create(this.fillInLinks(state.inputs, state.resource.inputs))
        .then((outputs) => {
          const instance: ResourceInstance<PropertyMap> = {
            desiredState: state,
            outputs,
          };
          clearTimeout(timerId);
          res(instance);
        })
        .catch((err) => {
          clearTimeout(timerId);
          rej(err);
        });
      return created;
    });
  }

  private fillInLinks(
    inputs: Partial<PropertyValues<PropertyMap>>,
    definitions: PropertyMap
  ): PropertyValues<PropertyMap> {
    return Object.keys(definitions).reduce((acc, key) => {
      const linkFillVisitor = new LinkFillVisitor(
        inputs[key],
        this.getLinkValue
      );
      acc[key] = acceptPropertyType(linkFillVisitor, definitions[key].type);
      return acc;
    }, {} as PropertyValues<PropertyMap>);
  }

  private getLinkValue = (resourceLink: ResourceLink): any => {
    const linkedValue = this.getNodeForState(resourceLink.item);
    if (!linkedValue.output) {
      throw new Error('Dependent state should already be created');
    }

    return resourceLink.outputAccessor(linkedValue.output.outputs);
  };

  private notifyItemSuccess(instance: ResourceInstance<PropertyMap>) {
    if (this.options?.onCreate) {
      try {
        this.options.onCreate(instance);
      } catch {
        // Prevent consumer errors stopping the generator
      }
    }
  }

  private notifyItemError(error: Error, desired: DesiredState) {
    if (this.options?.onError) {
      try {
        this.options.onError(new GenerationError(error, desired));
      } catch {
        // Prevent consumer errors stopping the generator
      }
    }
  }

  private get anyInProgress(): boolean {
    return this.inProgressCount > 0;
  }

  private getNextForCreation(count: number): DesiredState[] {
    const toFetch = count - this.inProgressCount;
    if (toFetch <= 0) {
      return [];
    }

    const upNext = this.queued.splice(0, count);
    return upNext.map(({ state }) => state);
  }

  private completeGeneration(): void {
    const [numCompleted, numErrored, total] = this.stateNodes.reduce(
      ([comp, err, tot], node) => [
        comp + (node.output ? 1 : 0),
        err + (node.error ? 1 : 0),
        tot + 1,
      ],
      [0, 0, 0] as [number, number, number]
    );
    if (numErrored > 0) {
      const errors = this.stateNodes
        .map((n) => n.error as GenerationError)
        .filter((n) => n);
      this.reject(
        new GenerationResultError('Generation encountered errors', errors)
      );
    } else if (numCompleted < total) {
      this.reject(new GenerationResultError('Generation stalled'));
    } else {
      this.resolve(
        this.stateNodes.map((n) => n.output as ResourceInstance<PropertyMap>)
      );
    }
  }

  private markCreating(_: DesiredState): void {
    this.inProgressCount++;
  }

  private markCreated(
    state: DesiredState,
    output: ResourceInstance<PropertyMap>
  ): void {
    const node = this.stateNodes.find((n) => n.state === state);
    if (!node) {
      throw new Error('Node does not exist');
    }

    node.output = output;
    this.inProgressCount--;
    this.appendReadyNodesToQueue(node.depedendents);
  }

  private markFailed(state: DesiredState, error: Error): void {
    const node = this.stateNodes.find((n) => n.state === state);
    if (!node) {
      throw new Error('Node does not exist');
    }

    node.error =
      error instanceof GenerationError
        ? error
        : new GenerationError(error, state);
    this.inProgressCount--;
  }

  private appendReadyNodesToQueue(nodes: StateNode[]): void {
    const ready = nodes.filter(
      (n) =>
        !n.output &&
        n.dependencies.every((dep) => dep.output) &&
        !this.queued.includes(n)
    );

    // TODO: Proper priority
    this.queued = this.queued
      .concat(ready)
      .sort((n1, n2) => n1.depth - n2.depth);
  }

  static create(state: DesiredState[], options?: GeneratorOptions): Generator {
    state = fillInDesiredStateTree(state);
    return new Generator(Generator.getStructure(state), options);
  }

  private getNodeForState = getNode(this.stateNodes);

  private static getStructure(stateValues: DesiredState[]): StateNode[] {
    const nodes: StateNode[] = stateValues.map((state) => ({
      state,
      depth: 0,
      created: false,
      dependencies: [],
      depedendents: [],
    }));

    for (const node of nodes) {
      for (const inputKey of Object.keys(node.state.resource.inputs)) {
        const inputDef = node.state.resource.inputs[inputKey];
        const resourceLinkVisitor = new ResourceLinkVisitor(
          node.state.inputs[inputKey]
        );
        const links = acceptPropertyType<ResourceLink[]>(
          resourceLinkVisitor,
          inputDef.type
        );
        for (const link of links) {
          const linkNode = getNode(nodes)(link.item);
          node.dependencies.push(linkNode);
          linkNode.depedendents.push(node);
        }
      }
    }

    return nodes;
  }
}

export class GenerationError extends Error {
  constructor(public inner: Error, public desired: DesiredState) {
    super();
    this.message = `Failed to create state: ${inner.message}`;
  }
}

export class GenerationResultError extends Error {
  constructor(message: string, public errors?: GenerationError[]) {
    super(message);
  }
}
